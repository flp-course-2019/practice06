name := "practice06"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.0" % "test"

lazy val root = (project in file("."))
  .enablePlugins(RunAcceptance)
  .settings(
    inConfig(AcceptanceTest)(
      Seq(acceptanceTestsPath := "exercises")
    )
  )